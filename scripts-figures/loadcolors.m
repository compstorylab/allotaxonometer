%% creates universal struct called colors

colors.blue = [43 103 198]/255;
colors.red = [198 43 103]/255;

colors.paleblue = [195 230 243]/255;
colors.palered = [255 204 204]/255;

colors.lightergrey = 0.96*[1 1 1];
colors.lightishgrey = 0.93*[1 1 1];
colors.lightgrey = 0.90*[1 1 1];

colors.lightgreyer = 0.85*[1 1 1];
colors.lightgreyish = 0.80*[1 1 1];

colors.grey = 0.75*[1 1 1];
colors.darkgrey = 0.55*[1 1 1];
colors.darkergrey = 0.35*[1 1 1];
colors.verydarkgrey = 0.15*[1 1 1];
colors.superdarkgrey = 0.10*[1 1 1];
colors.reallyverdarkgrey = 0.05*[1 1 1];

colors.orange = [255 116 0]/255;