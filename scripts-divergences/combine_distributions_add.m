function elements_added = combine_distributions_add(elements)
%% elements_added = combine_distributions_add(elements)
%% 
%% straight combination of distributions by adding counts
%% 
%% expects input of the form of a vector structures of matching form
%% 
%% two pieces are required for each system:
%% 
%% elements(i).types (cell array, names of things)
%% elements(i).counts (counts of things, more generally may be sizes
%% of things)
%% 
%% computed:
%% 
%% %% 
%% optional:
%% 
%% elements_added.ranks (ranks of things based on counts or sizes)
%% if present in elements(i)
%% elements1.probs ( = elements1.counts/elements1.totalcount)
%% elements_added.totalcount (overall count for normalization)

%% produces:
%% 
%% elements3 of same form

elements3.types = ...
    union(elements1.types,...
          elements2.types,...
          'stable');

N = length(elements3.types);

%% first distribution

[presence,indices] = ...
    ismember(elements3.types,...
             elements1.types);
newindices = find(presence==1);

elements3.counts = zeros(N,1);
elements3.counts(newindices) = elements1.counts(indices(newindices));

if (isfield(elements1,'probs'))
    elements3.probs = zeros(N,1);
    elements3.probs(newindices) = ...
        0.5*elements1.probs(indices(newindices));
end

if (isfield(elements1,'totalcounts'))
    elements3.totalcounts = elements1.totalcounts;
end

%% second distribution

[presence,indices] = ...
    ismember(elements3.types,...
             elements2.types);
newindices = find(presence==1);

elements3.counts(newindices) = elements3.counts(newindices) + ....
    elements2.counts(indices(newindices));

if (isfield(elements2,'probs'))
    elements3.probs(newindices) = ...
        elements3.probs(newindices) + ...
        elements2.probs(indices(newindices));
end

if (isfield(elements2,'totalcounts'))
    elements3.totalcounts = elements3.totalcounts + elements2.totalcounts;
end


%% compute ranks
elements3.ranks = tiedrank(-elements3.counts);
