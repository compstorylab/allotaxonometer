function [ranks,flux] = rank_turbulence_flux(mixedelements)
%% [ranks,flux] = rank_turbulence_flux(mixedelements)
%% 
%% returns complete rank turbulence flux and corresponding ranks
%% 
%% ranks = union of mixedelements(1).ranks 
%%         and mixedelements(2).ranks2 
%%         filtered through unique
%% 
%% only returns a general flux as upwards flux and downwards flux
%% are equal
%% 
%% (for probability boundaries, these fluxes are likely distinct
%% and both fluxes must be determined and returned)
%% 
%% see also:
%% rank_turbulence_flux_sampler (for a small number of specified ranks)
%% rank_turbulence_flux_element_crossings (returns types crossing a
%% given rank)

ranks = sort(unique([col(mixedelements(1).ranks); col(mixedelements(2).ranks)]),'ascend');

%% create container for ranks to retrieve indices in ranks vector
ranks2indices_Map = containers.Map(ranks,(1:length(ranks))');

%% initialize differential upwards flux
fluxupdiff = zeros(size(ranks));

%% find upwards flux types:
indices_fluxup = find(mixedelements(2).ranks < mixedelements(1).ranks);

indices_ranks1_fluxup = cell2mat(values(ranks2indices_Map,num2cell(mixedelements(1).ranks(indices_fluxup))));
indices_ranks2_fluxup = cell2mat(values(ranks2indices_Map,num2cell(mixedelements(2).ranks(indices_fluxup))));
    
for i=1:length(indices_fluxup)
    fluxupdiff(indices_ranks1_fluxup(i)) = ...
        fluxupdiff(indices_ranks1_fluxup(i)) - 1;
    fluxupdiff(indices_ranks2_fluxup(i)) = ...
        fluxupdiff(indices_rank2_fluxup(i)) + 1;
end

flux = cumsum(fluxupdiff);
