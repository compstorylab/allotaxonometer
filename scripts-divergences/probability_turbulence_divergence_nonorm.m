function [divergence_elements] = probability_turbulence_divergence_nonorm(p1,p2,alpha)
%% [divergence_elements] = probability_turbulence_divergence_nonorm(p1,p2,alpha)
%% 
%% returns per type, un-normed probility turbulence adivergence values for
%% given alpha for arrays of p1 and p2
%% 
%% - accompaniment to probability_turbulence_divergence
%% - for use in constructing contour lines on allotaxonographs
%% 
%% alpha >= 0 and may be specified as Inf

if (alpha < 0)
    error('alpha must be >= 0');
elseif (alpha == Inf)
    divergence_elements = max(p1,p2);
    divergence_elements(find(p1==p2)) = 0;
elseif (alpha == 0)
    %% 1s or 0s, 1/alpha limit removed
    divergence_elements = ...
        (p1==0) + (p2==0);
else
    divergence_elements = ...
        (alpha+1)/alpha* ...
        (abs(p1.^alpha - p2.^alpha)).^(1./(alpha+1));
end

