Set up:

Optional
Run 

/Applications/MATLAB_R2019b.app/bin/matlab -nodesktop -nosplash

---------------------------------------
%% add all subdirectories in ~/matlab
paths_to_add = genpath('~/matlab');

%% remove .git and archive subdirectories
paths_to_add = regexprep(paths_to_add,'~\/matlab[^:]*?\/\.git.*?:','','all');
paths_to_add = regexprep(paths_to_add,'~\/matlab\/archive.*?:','','all');

%% add paths
addpath(paths_to_add);
---------------------------------------
